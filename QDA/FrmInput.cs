﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QDA
{
    public partial class FrmInput : Form
    {
        public static string Show(IWin32Window owner, string message, string @default)
        {
            using (var frm = new FrmInput())
            {
                frm.label1.Text = message;
                frm.textBox1.Text = @default;

                if (frm.ShowDialog(owner) == DialogResult.OK)
                {
                    return frm.textBox1.Text;
                }

                return null;
            }
        }

        public FrmInput()
        {
            InitializeComponent();
        }
    }
}
