﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QDA
{
    class TopicList : IEnumerable<Topic>
    {
        List<Topic> all = new List<Topic>();
        Dictionary<string, Topic> byId = new Dictionary<string, Topic>();

        public void Add(Topic topic)
        {
            all.Add(topic);
            byId.Add(topic.id, topic);
        }

        public Topic this[string id]
        {
            get { return byId[id]; }
        }

        public Topic this[int index]
        {
            get { return all[index]; }
        }

        public int Count
        {
            get { return all.Count; }
        }

        public IEnumerator<Topic> GetEnumerator()
        {
            return all.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
