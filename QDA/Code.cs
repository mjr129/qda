﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QDA
{
    class Code
    {
        public string Id;
        public string Description;

        public Code(string id, string description)
        {
            this.Id = id;
            this.Description = description;
        }

        public bool IsIcrCode
        {
            get
            {
                return Description.Contains("[ICR]");
            }
        }

        public bool IsCategory
        {
            get
            {
                return Id.StartsWith("_") && Id.EndsWith("_");
            }
        }

        public bool IsMeta
        {
            get
            {
                return Description.StartsWith("~");
            }
        }

        public string[] GetMetaContent()
        {
            return Description.Substring(1).Split("~".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }

        public string DisplayName
        {
            get
            {
                string suffix = (IsIcrCode || IsMeta) ? "*" : "";

                if (IsCategory)
                {
                    return "-----" + Id.Replace("_", " ") + suffix + "-----";
                }
                else
                {
                    return Id + suffix;
                }
            }
        }
    }
}
