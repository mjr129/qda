﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QDA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            boldFont = new Font(Font, FontStyle.Bold);
            strikeFont = new Font(Font, FontStyle.Strikeout);
            italicFont = new Font(Font, FontStyle.Italic);
            normalFont = Font;
            smallFont = new Font("Arial", 6, FontStyle.Regular);
            smallBoldFont = new Font(smallFont, FontStyle.Bold);

            SetTitleText();
        }

        List<Code> codes = new List<Code>();
        TopicList mytopics = new TopicList();
        TopicList icrtopics = new TopicList();
        TopicList distopics = new TopicList();

        Font smallFont;
        Font smallBoldFont;
        Font boldFont;
        Font strikeFont;
        Font italicFont;
        Font normalFont;

        private void LoadData(string fn)
        {
            _fileName = fn;

            using (StreamReader sr = new StreamReader(fn))
            {
                string line = sr.ReadLine();
                Assert(line == "#ENTRY");

                while (!sr.EndOfStream)
                {
                    string id = sr.ReadLine();
                    string url = sr.ReadLine();
                    string title = sr.ReadLine();
                    string user = sr.ReadLine();
                    StringBuilder post = new StringBuilder();

                    for (string postLine = sr.ReadLine(); postLine != "#ENTRY" && !sr.EndOfStream; postLine = sr.ReadLine())
                    {
                        post.AppendLine(postLine);
                    }

                    Topic topic = new Topic(id, url, title, user, post.ToString(), mytopics.Count + 1);
                    mytopics.Add(topic);
                    icrtopics.Add(new Topic(topic));
                    distopics.Add(new Topic(topic));
                }
            }

            _dataFileName = GetDataFile(fn);

            if (File.Exists(_dataFileName))
            {
                using (StreamReader sr = new StreamReader(_dataFileName))
                {
                    codes.Clear();

                    string l = sr.ReadLine();

                    while (!string.IsNullOrWhiteSpace(l) && !sr.EndOfStream)
                    {
                        string[] e = l.Split("=".ToCharArray(), 2);

                        Code code = new Code(e[0], e.Length >= 2 ? e[1] : "");
                        codes.Add(code);

                        l = sr.ReadLine();
                    }

                    while (!sr.EndOfStream)
                    {
                        l = sr.ReadLine();

                        if (!string.IsNullOrWhiteSpace(l))
                        {
                            string[] e = l.Split("=".ToCharArray(), 2);

                            string c = e[1];

                            if (!c.Contains("¬"))
                            {
                                c += "¬";
                            }

                            string id = e[0];
                            string[] cs = c.Split("¬".ToCharArray(), 2);
                            string[] yc = cs[0].Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                            string[] nc = cs[1].Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                            TopicList list = mytopics;

                            if (id.Contains("|"))
                            {
                                string sub = id.Substring(id.IndexOf('|') + 1);
                                id = id.Substring(0, id.IndexOf('|'));

                                if (sub == "ICR")
                                {
                                    list = icrtopics;
                                }
                                else
                                {
                                    throw new NotImplementedException("No such list as \"" + sub + "\"");
                                }
                            }

                            list[id].yescodes.Clear();
                            list[id].yescodes.AddRange(yc);

                            list[id].nocodes.Clear();
                            list[id].nocodes.AddRange(nc);
                        }
                    }
                }
            }

            UpdateTopics(true);

            listView1.Columns[0].Width = 32;
            listView1.Columns[1].Width = 64;
            listView1.Columns[2].Width = 0;
            listView1.Columns[3].Width = 64;
            listView1.Columns[4].Width = 32;

            SetTitleText();
        }

        private TopicList topics
        {
            get
            {
                if (this.checkBox1.Checked)
                {
                    return icrtopics;
                }
                else if (this.checkBox2.Checked)
                {
                    return distopics;
                }
                else
                {
                    return mytopics;
                }
            }
        }

        private bool PassesFilter(Topic t)
        {
            if (filterWithCodes == CheckState.Checked && t.yescodes.Count == 0)
            {
                return false;
            }

            if (filterWithCodes == CheckState.Indeterminate && t.yescodes.Count != 0)
            {
                return false;
            }

            if (notfilter.Count != 0 && !notfilter.TrueForAll(z => !t.yescodes.Contains(z)))
            {
                return false;
            }

            return filter.Count == 0 || filter.TrueForAll(z => t.yescodes.Contains(z));
        }

        string _dataFileName;

        protected override void OnClosing(CancelEventArgs e)
        {
            SaveAll();
        }

        private void SaveAll()
        {
            if (_dataFileName != null)
            {
                using (StreamWriter sw = new StreamWriter(_dataFileName))
                {
                    foreach (Code code in codes)
                    {
                        sw.WriteLine(code.Id + "=" + code.Description);
                    }

                    sw.WriteLine();

                    foreach (Topic t in mytopics)
                    {
                        sw.WriteLine(t.id + "=" + string.Join("|", t.yescodes) + "¬" + string.Join("|", t.nocodes));
                    }

                    foreach (Topic t in icrtopics)
                    {
                        sw.WriteLine(t.id + "|ICR=" + string.Join("|", t.yescodes) + "¬" + string.Join("|", t.nocodes));
                    }
                }
            }
        }

        private void UpdateListItem(ListViewItem lvi)
        {
            Topic t = (Topic)lvi.Tag;
            lvi.SubItems[0].Text = (t.id);
            lvi.SubItems[1].Text = (t.title);
            lvi.SubItems[2].Text = (t.user);
            lvi.SubItems[3].Text = (string.Join(", ", t.yescodes));
            lvi.SubItems[4].Text = (t.index.ToString());
            lvi.ForeColor = t.yescodes.Count == 0 ? Color.DarkGray : Color.Black;
        }

        private string GetDataFile(string fn)
        {
            return fn + ".codes";
        }

        private void Assert(bool p)
        {
            if (!p)
            {
                throw new NotImplementedException();
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTopicView();
        }

        private void UpdateTopicView()
        {
            Topic topic = SelectedTopic;

            string fn = Path.Combine(Application.StartupPath, "temp.htm");

            using (StreamWriter sw = new StreamWriter(fn))
            {
                sw.WriteLine("<html><body>");
                ToHtml(topic, sw);
                sw.WriteLine("</body></html>");
            }

            webBrowser1.Navigate(fn);

            UpdateCodesList();
        }

        private static void ToHtml(Topic topic, StreamWriter sw)
        {
            sw.WriteLine("<h2><a href=\"" + topic.url + "\">" + topic.title + "</a></h2>");
            sw.WriteLine("<h3>" + topic.user + "</h3>");
            sw.WriteLine(topic.post);
        }

        private static void ToHtml(Topic topic, int index, string codes, StreamWriter sw)
        {
            sw.WriteLine("<h2>" + topic.id + ". " + topic.title + "</a></h2>");
            sw.WriteLine("<h3><i>Codes: </i>" + codes.ToLower() + "</h3>");
            sw.WriteLine(topic.post);
        }

        class ListCode
        {
            public string code;
            public bool removed;

            public ListCode(string code, bool removed)
            {
                this.code = code;
                this.removed = removed;
            }

            public ListCode(string code)
            {
                this.code = code;
                this.removed = false;
            }

            public override string ToString()
            {
                if (removed)
                {
                    return code + " [REMOVED]";
                }
                else
                {
                    return code;
                }
            }

        }

        Topic _codesListTopic;
        bool updateingCodesList = false;

        private void UpdateCodesList()
        {
            updateingCodesList = true;

            Topic topic = SelectedTopic;

            _codesListTopic = topic;
            label1.Text = topic.id + ": " + topic.title;

            listView2.Items.Clear();

            bool ficrView = checkBox2.Checked;
            bool icrView = !ficrView && iCRComparetoggleToolStripMenuItem.Checked;

            foreach (Code code in codes)
            {
                ListViewItem lvi = new ListViewItem(code.DisplayName);

                if (topic.id != "")
                {
                    if (icrView)
                    {
                        Topic alt;

                        if (topics == mytopics)
                        {
                            alt = icrtopics[topic.id];
                        }
                        else
                        {
                            alt = mytopics[topic.id];
                        }

                        if (alt.yescodes.Contains(code.Id))
                        {
                            lvi.Text = "☑ " + lvi.Text;
                        }
                        else
                        {
                            lvi.Text = "☐ " + lvi.Text;
                        }
                    }
                    else if (ficrView)
                    {
                        Topic alt1 = mytopics[topic.id];
                        Topic alt2 = icrtopics[topic.id];

                        if (alt2.yescodes.Contains(code.Id))
                        {
                            lvi.Text = "☑ " + lvi.Text;
                        }
                        else
                        {
                            lvi.Text = "☐ " + lvi.Text;
                        }

                        if (alt1.yescodes.Contains(code.Id))
                        {
                            lvi.Text = "☑" + lvi.Text;
                        }
                        else
                        {
                            lvi.Text = "☐" + lvi.Text;
                        }
                    }
                }

                lvi.SubItems.Add(code.Description);
                lvi.Tag = code.Id;
                lvi.Checked = topic.yescodes.Contains(code.Id);

                bool anySet = lvi.Checked || topic.nocodes.Contains(code.Id);
                SetItemState(lvi, lvi.Checked ? CheckState.Checked : anySet ? CheckState.Unchecked : CheckState.Indeterminate);

                listView2.Items.Add(lvi);
            }

            foreach (string code in topic.yescodes)
            {
                if (!codes.Exists(z => z.Id == code))
                {
                    ListViewItem lvi = new ListViewItem(code + " [REMOVED]");
                    lvi.Tag = code;
                    lvi.Checked = true;
                    SetItemState(lvi, CheckState.Checked, true);

                    listView2.Items.Add(lvi);
                }
            }

            foreach (string code in topic.nocodes)
            {
                if (!codes.Exists(z => z.Id == code))
                {
                    ListViewItem lvi = new ListViewItem(code + " [REMOVED]");
                    lvi.Tag = code;
                    lvi.Checked = false;
                    SetItemState(lvi, CheckState.Unchecked, true);

                    listView2.Items.Add(lvi);
                }
            }

            listView2.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            listView2.Columns[1].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);

            updateingCodesList = false;
        }

        private Topic SelectedTopic
        {
            get
            {
                Topic topic;

                if (listView1.SelectedItems.Count == 0)
                {
                    topic = new Topic("", "", "", "", "", 0);
                }
                else
                {
                    topic = (Topic)listView1.SelectedItems[0].Tag;
                }

                return topic;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string code = FrmInput.Show(this, "New code", "");

            if (code != null && !codes.Exists(z => z.Id == code))
            {
                string desc = FrmInput.Show(this, "Description", "");

                if (desc != null)
                {
                    codes.Add(new Code(code, desc));

                    SelectedTopic.SetCode(code, CheckState.Checked);

                    UpdateCodesList();

                    Debug.WriteLine(_codesListTopic.id + ": new code " + code + " is true");

                    foreach (ListViewItem lvi in listView1.SelectedItems)
                    {
                        UpdateListItem(lvi);
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView2.SelectedItems)
            {
                string code = (string)lvi.Tag;
                codes.RemoveAll(z => z.Id == code);
            }

            UpdateCodesList();
        }

        /*
        private void ApplyCodesList()
        {
            if (_codesListTopic != null)
            {
                for (int n = 0; n < checkedListBox1.Items.Count; n++)
                {
                    ListCode cl = (ListCode)checkedListBox1.Items[n];
                    bool c = checkedListBox1.GetItemChecked(n);

                    _codesListTopic.SetCode(cl.code, c);
                }
            }
        } */

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView2.SelectedItems)
            {
                string code = (string)lvi.Tag;

                foreach (Code c in codes)
                {
                    if (c.Id == code)
                    {
                        string newDesc = FrmInput.Show(this, c.Id, c.Description);

                        if (newDesc != null)
                        {
                            c.Description = newDesc;
                            lvi.SubItems[1].Text = newDesc;
                        }
                    }
                }
            }
        }

        void SetItemState(ListViewItem lvi, CheckState cs, bool pendingRemoval = false)
        {
            switch (cs)
            {
                case CheckState.Checked:
                    lvi.ForeColor = Color.Black;
                    lvi.Font = pendingRemoval ? italicFont : normalFont;
                    return;

                case CheckState.Unchecked:
                    lvi.ForeColor = Color.DarkRed;
                    lvi.Font = pendingRemoval ? italicFont : normalFont;
                    return;

                case CheckState.Indeterminate:
                    lvi.ForeColor = pendingRemoval ? Color.Gray : Color.Goldenrod;
                    lvi.Font = pendingRemoval ? strikeFont : boldFont;
                    return;
            }
        }

        private void listView2_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (!updateingCodesList)
            {
                if (_codesListTopic != null)
                {
                    ListView lv = (ListView)sender;

                    string code = (string)lv.Items[e.Index].Tag;
                    Code ccode = GetCode(code);

                    if (ccode != null)
                    {
                        if (checkBox1.Checked && ccode.IsIcrCode)
                        {
                            e.NewValue = e.CurrentValue;
                        }

                        if (ccode.IsCategory || ccode.IsMeta)
                        {
                            e.NewValue = e.CurrentValue;
                        }
                    }
                }
            }
        }

        private void listView2_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!updateingCodesList)
            {
                if (_codesListTopic != null)
                {
                    string code = (string)e.Item.Tag;
                    bool isChecked = e.Item.Checked;
                    CheckState state;

                    if (!codes.Exists(z => z.Id == code))
                    {
                        state = CheckState.Indeterminate;
                        updateingCodesList = true;
                        e.Item.Checked = false;
                        updateingCodesList = false;
                        SetItemState(e.Item, state, true);
                    }
                    else
                    {
                        state = isChecked ? CheckState.Checked : CheckState.Unchecked;
                        SetItemState(e.Item, state);
                    }

                    _codesListTopic.SetCode(code, state);

                    Debug.WriteLine(_codesListTopic.id + ": code " + code + " is " + isChecked);
                    SaveAll();

                    foreach (ListViewItem lvi in listView1.SelectedItems)
                    {
                        UpdateListItem(lvi);
                    }
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        List<string> filter = new List<string>();
        List<string> notfilter = new List<string>();
        private CheckState filterWithCodes;
        private string _fileName;

        private Dictionary<string, int> CountTopics()
        {
            Dictionary<string, int> r = new Dictionary<string, int>();
            r.Add("=TOTAL", 0);
            r.Add("=WITHCODES", 0);

            foreach (Topic t in topics)
            {
                if (PassesFilter(t))
                {
                    foreach (string code in t.yescodes)
                    {
                        if (r.ContainsKey(code))
                        {
                            r[code]++;
                        }
                        else
                        {
                            r.Add(code, 1);
                        }
                    }

                    r["=TOTAL"]++;

                    if (t.yescodes.Count != 0)
                    {
                        r["=WITHCODES"]++;
                    }
                }
            }

            return r;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Items.Clear();

            var counts = CountTopics();

            ToolStripMenuItem tsmi = new ToolStripMenuItem("All");
            tsmi.Font = smallBoldFont;
            tsmi.ShortcutKeyDisplayString = counts["=TOTAL"].ToString();
            tsmi.Click += clearFilter_Click;

            if (filter.Count != 0 || notfilter.Count != 0 || filterWithCodes != CheckState.Unchecked)
            {
                tsmi.Text = "Current filter";
            }

            contextMenuStrip1.Items.Add(tsmi);

            //
            int withCodes = counts["=WITHCODES"];
            tsmi = new ToolStripMenuItem("With codes");
            tsmi.Font = smallBoldFont;
            tsmi.ShortcutKeyDisplayString = withCodes.ToString();

            tsmi.CheckState = filterWithCodes;
            tsmi.Click += withCodes_Click;

            contextMenuStrip1.Items.Add(tsmi);

            foreach (Code code in codes)
            {
                if (code.IsCategory)
                {
                    var tsl = new ToolStripSeparator();
                    contextMenuStrip1.Items.Add(tsl);
                }
                //else
                //{
                tsmi = new ToolStripMenuItem(code.Id);

                if (counts.ContainsKey(code.Id))
                {
                    tsmi.ShortcutKeyDisplayString = counts[code.Id].ToString() + " (" + ((counts[code.Id] * 100d) / withCodes).ToString("F0") + "%)";
                }

                tsmi.Checked = filter.Contains(code.Id);

                if (notfilter.Contains(code.Id))
                {
                    tsmi.CheckState = CheckState.Indeterminate;
                }

                tsmi.Font = smallFont;
                tsmi.AutoSize = false;
                tsmi.Height = 16;
                tsmi.Width = 256;
                tsmi.Click += filterCode_Click;

                contextMenuStrip1.Items.Add(tsmi);
                //}
            }

            SetTitleText();

            contextMenuStrip1.Show(button5, new Point(0, 0), ToolStripDropDownDirection.AboveRight);
        }

        private void SetTitleText()
        {
            Text = "QDA - " + Path.GetFileName(_fileName) + " - " + FilterToString();
        }

        private void withCodes_Click(object sender, EventArgs e)
        {
            switch (filterWithCodes)
            {
                case CheckState.Checked:
                    filterWithCodes = CheckState.Indeterminate;
                    break;

                case CheckState.Indeterminate:
                    filterWithCodes = CheckState.Unchecked;
                    break;

                case CheckState.Unchecked:
                    filterWithCodes = CheckState.Checked;
                    break;
            }

            UpdateTopics(true);
            button5.PerformClick();
        }

        private void clearFilter_Click(object sender, EventArgs e)
        {
            filter.Clear();
            notfilter.Clear();

            UpdateTopics(true);
            button5.PerformClick();
        }

        void filterCode_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;

            if (filter.Contains(tsmi.Text))
            {
                filter.Remove(tsmi.Text);
                notfilter.Add(tsmi.Text);
            }
            else if (notfilter.Contains(tsmi.Text))
            {
                notfilter.Remove(tsmi.Text);
            }
            else
            {
                filter.Add(tsmi.Text);
            }

            UpdateTopics(true);
            button5.PerformClick();
        }

        private Code GetCode(string c)
        {
            foreach (Code code in codes)
            {
                if (code.Id == c)
                {
                    return code;
                }
            }

            return null;
        }

        private bool DoesCodeExist(string c)
        {
            foreach (Code code in codes)
            {
                if (code.Id == c)
                {
                    return true;
                }
            }

            return false;
        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private static string GetFilename(string fileName)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                string ext = Path.GetExtension(fileName);
                sfd.Filter = ext.Substring(1).ToUpper() + " files (*" + ext + ")|*" + ext;
                sfd.InitialDirectory = Path.GetDirectoryName(fileName);
                sfd.FileName = Path.GetFileName(fileName);

                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    return sfd.FileName;
                }
            }

            return null;
        }

        private string FilterToString()
        {
            StringBuilder sb = new StringBuilder();
            bool comma = false;

            if (filterWithCodes == CheckState.Checked)
            {
                sb.Append("(with codes)");
                comma = true;
            }
            else if (filterWithCodes == CheckState.Indeterminate)
            {
                sb.Append("(without codes)");
                comma = true;
            }

            foreach (string code in filter)
            {
                if (comma) sb.Append(","); else comma = true;

                sb.Append(code);
            }

            foreach (string code in notfilter)
            {
                if (comma) sb.Append(","); else comma = true;

                sb.Append("!" + code);
            }

            if (sb.Length == 0)
            {
                return "(no filter)";
            }

            return sb.ToString();
        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs ev)
        {

        }

        private void UpdateTopics(bool listChanged = false)
        {
            if (listChanged)
            {
                listView1.Items.Clear();

                foreach (Topic t in topics)
                {
                    if (PassesFilter(t))
                    {
                        ListViewItem lvi = new ListViewItem();
                        lvi.SubItems.Add(new ListViewItem.ListViewSubItem());
                        lvi.SubItems.Add(new ListViewItem.ListViewSubItem());
                        lvi.SubItems.Add(new ListViewItem.ListViewSubItem());
                        lvi.SubItems.Add(new ListViewItem.ListViewSubItem());
                        lvi.Tag = t;
                        UpdateListItem(lvi);
                        listView1.Items.Add(lvi);
                    }
                }
            }
            else
            {
                foreach (ListViewItem lvi in listView1.Items)
                {
                    UpdateListItem(lvi);
                }
            }

            UpdateTopicView();
        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            HandleIcrMode();

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            HandleIcrMode();
        }

        private void HandleIcrMode()
        {
            if (checkBox1.Checked)
            {
                label1.ForeColor = Color.FromArgb(64, 64, 64);
                label1.BackColor = Color.Pink;
            }
            else if (checkBox2.Checked)
            {
                label1.ForeColor = Color.FromArgb(255, 255, 0);
                label1.BackColor = Color.Black;
            }
            else
            {
                label1.ForeColor = Color.FromArgb(64, 64, 64);
                label1.BackColor = Color.LightBlue;
            }

            // Set [ICR] codes in mytopics --> icrtopics
            foreach (Code c in codes)
            {
                if (c.IsIcrCode)
                {
                    foreach (Topic t in mytopics)
                    {
                        CheckState s = t.GetCode(c.Id);
                        mytopics[t.id].SetCode(c.Id, s);
                        icrtopics[t.id].SetCode(c.Id, s);
                    }
                }
            }

            // Set discrepancies in distopics
            foreach (Topic t in mytopics)
            {
                Topic t2 = icrtopics[t.id];
                Topic t3 = distopics[t.id];
                t3.yescodes.Clear();
                t3.nocodes.Clear();

                foreach (string code in t.yescodes.Union(t2.yescodes))
                {
                    if (t.yescodes.Contains(code) && t2.yescodes.Contains(code))
                    {
                        t3.SetCode(code, CheckState.Checked);
                    }
                    else
                    {
                        t3.SetCode(code, CheckState.Unchecked);
                    }
                }
            }

            Topic st = SelectedTopic;

            UpdateTopics(true);

            foreach (ListViewItem lvi in listView1.Items)
            {
                if (((Topic)lvi.Tag).id == st.id)
                {
                    lvi.Selected = true;
                    break;
                }
            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void loadDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    LoadData(ofd.FileName);
                    loadDataToolStripMenuItem.Enabled = false;
                }
            }
        }

        private void saveCountsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileName = GetFilename(_fileName + ".report.csv");

            if (fileName == null)
            {
                return;
            }

            using (StreamWriter sw = new StreamWriter(fileName))
            {
                var counts = CountTopics();
                int total = counts["=TOTAL"];

                sw.WriteLine("\"code\",\"count\",\"percent\"");

                foreach (var kvp in counts)
                {
                    if (!DoesCodeExist(kvp.Key) && kvp.Key.StartsWith("="))
                    {
                        sw.Write("\"(info) " + kvp.Key + "\"");
                        sw.Write(",");
                        sw.Write(kvp.Value);
                        sw.Write(",");
                        sw.Write(((kvp.Value * 100d) / total).ToString("F1"));
                        sw.WriteLine();
                    }
                }

                foreach (Code c in codes)
                {
                    sw.Write("\"" + c.Id + "\"");
                    sw.Write(",");

                    if (counts.ContainsKey(c.Id))
                    {
                        sw.Write(counts[c.Id]);
                        sw.Write(",");
                        sw.Write(((counts[c.Id] * 100d) / total).ToString("F1"));
                    }
                    else
                    {
                        sw.Write(0);
                        sw.Write(",");
                        sw.Write(0);
                    }

                    sw.WriteLine();
                }

                sw.Write("\"# filter=\",\"");
                sw.Write(FilterToString());
                sw.WriteLine("\"");
            }

            Process.Start("explorer.exe", "/select,\"" + fileName + "\"");
        }

        private void saveCodesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileName = GetFilename(_fileName + ".export.csv");

            if (fileName == null)
            {
                return;
            }

            using (StreamWriter sw = new StreamWriter(fileName))
            {
                sw.WriteLine("Index,Title,Codes");

                int i = 0;

                foreach (Topic t in topics)
                {
                    if (PassesFilter(t))
                    {
                        i += 1;
                        sw.WriteLine(t.index + "," + t.title + ",\"" + string.Join(", ", t.yescodes) + "\"");
                    }
                }
            }

            Process.Start("explorer.exe", "/select,\"" + fileName + "\"");
        }

        private void saveHTMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileName = GetFilename(_fileName + ".viewall.htm");

            if (fileName == null)
            {
                return;
            }

            using (StreamWriter sw = new StreamWriter(fileName))
            {
                sw.WriteLine("<html><body>");

                int i = 0;

                foreach (Topic t in topics)
                {
                    if (PassesFilter(t))
                    {
                        i += 1;

                        StringBuilder c = new StringBuilder();

                        foreach (string code in t.yescodes)
                        {
                            if (DoesCodeExist(code))
                            {
                                if (c.Length != 0)
                                {
                                    c.Append(", ");
                                }

                                c.Append(code);
                            }
                        }


                        ToHtml(t, i, c.ToString(), sw);
                    }
                }

                sw.WriteLine("</body></html>");
            }

            Process.Start("explorer.exe", "/select,\"" + fileName + "\"");
        }

        private void categoriseToolStripMenuItem_Click(object sender, EventArgs ev)
        {
            if (MessageBox.Show(this, "Categorise? This will apply the heading (_xxxx_) code to each object if one or more code under that heading is set", "Categorise", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            foreach (Topic t in topics)
            {
                foreach (Code c in codes)
                {
                    if (c.IsMeta)
                    {
                        string[] e = c.GetMetaContent();

                        bool isAny = false;

                        foreach (string ec in e)
                        {
                            if (t.yescodes.Contains(ec))
                            {
                                isAny = true;
                                break;
                            }
                        }

                        if (t.yescodes.Count != 0)
                        {
                            t.SetCode(c.Id, isAny ? CheckState.Checked : CheckState.Unchecked);
                        }
                    }
                }
            }

            foreach (Topic t in topics)
            {
                Code h = null;
                bool a = false;
                bool af = false;

                foreach (Code c in codes)
                {
                    if (c.IsCategory)
                    {
                        if (h != null)
                        {
                            t.SetCode(h.Id, a ? CheckState.Checked : af ? CheckState.Unchecked : CheckState.Indeterminate);
                        }

                        h = c;
                        a = false;
                    }
                    else if (t.yescodes.Contains(c.Id))
                    {
                        a = true;
                    }
                    else if (t.nocodes.Contains(c.Id))
                    {
                        af = true;
                    }
                }

                if (h != null)
                {
                    t.SetCode(h.Id, a ? CheckState.Checked : CheckState.Unchecked);
                }
            }

            UpdateTopics();
        }

        private void button11_Click(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void calculateICRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileName = GetFilename(_fileName + ".ICR.csv");

            if (fileName == null)
            {
                return;
            }

            using (StreamWriter sw = new StreamWriter(fileName))
            {
                List<string> allCodes = codes.ConvertAll(z => z.Id);
                int t = 0;
                int tm = 0;

                sw.WriteLine("ID,Title,MyCodes,IcrCodes,Matches");

                foreach (Topic mo in topics)
                {
                    if (PassesFilter(mo))
                    {
                        Topic m = mytopics[mo.id];
                        sw.Write(m.id + ",\"" + m.title + "\",");

                        Topic i = icrtopics[m.id];

                        List<string> ucodes = new List<string>(m.yescodes.Union(i.yescodes));

                        ucodes.Sort((x, y) =>
                            (m.yescodes.Contains(x) && i.yescodes.Contains(x))
                                ? ((m.yescodes.Contains(y) && i.yescodes.Contains(y)) ? allCodes.IndexOf(x).CompareTo(allCodes.IndexOf(y)) : -1)
                                : (((m.yescodes.Contains(y) && i.yescodes.Contains(y)) ? 1 : allCodes.IndexOf(x).CompareTo(allCodes.IndexOf(y)))));

                        bool needsNewLine = false;

                        foreach (string code in ucodes)
                        {
                            Code acode = GetCode(code);

                            if (acode == null || acode.IsIcrCode || acode.IsMeta || acode.IsCategory)
                            {
                                continue;
                            }

                            if (needsNewLine)
                            {
                                sw.Write(",,");
                            }
                            else
                            {
                                needsNewLine = true;
                            }

                            bool a = m.yescodes.Contains(code);

                            if (a)
                            {
                                sw.Write(code);
                            }

                            sw.Write(",");

                            bool b = i.yescodes.Contains(code);

                            if (b)
                            {
                                sw.Write(code);
                            }

                            sw.Write(",");

                            t++;

                            if (a && b)
                            {
                                tm++;
                                sw.WriteLine("1");
                            }
                            else
                            {
                                sw.WriteLine("0");
                            }
                        } // codes
                    } // filter
                } // topic

                int tcd = 0;

                foreach (Code c in codes)
                {
                    if (!c.IsIcrCode && !c.IsMeta && !c.IsCategory)
                    {
                        tcd++;
                    }
                }

                double pa = (double)tm / t;
                double pc = (1d / tcd);
                double k = (pa - pc) / (1 - pc);

                sw.WriteLine(",");
                sw.WriteLine("\"Total codes\"," + t);
                sw.WriteLine("\"Total matched\"," + tm);
                sw.WriteLine("\"p(agree))\"," + pa.ToString("F3"));
                sw.WriteLine("\"Number of codes\"," + tcd);
                sw.WriteLine("\"p(chance)\"," + pc.ToString("F3"));
                sw.WriteLine("\"Cohen's Kappa\"," + k);
                sw.WriteLine(",");

                sw.WriteLine("Code,MyCount,IcrCount,Matches,Mismatches,Total");

                foreach (Code c in codes)
                {
                    int ca = 0;
                    int cb = 0;
                    int cc = 0;
                    int cd = 0;
                    int ct = 0;

                    foreach (Topic tt in mytopics)
                    {
                        if (PassesFilter(tt))
                        {
                            Topic tx = mytopics[tt.id];
                            Topic ty = icrtopics[tt.id];
                            bool bx = tx.yescodes.Contains(c.Id);
                            bool by = ty.yescodes.Contains(c.Id);

                            if (bx)
                            {
                                ca++;
                            }

                            if (by)
                            {
                                cb++;
                            }

                            if (bx && by)
                            {
                                cc++;
                            }

                            if (bx != by)
                            {
                                cd++;
                            }

                            if (bx || by)
                            {
                                ct++;
                            }
                        }
                    }

                    sw.WriteLine(c.Id + "," + ca + "," + cb + "," + cc + "," + cd + "," + ct);
                }

                MessageBox.Show(this, "Total matched (percent): " + (pa * 100d).ToString("F1") + "%\r\nCohen's Kappa (percent): " + (k * 100d).ToString("F1") + "%");
            } // steamwriter

            Process.Start("explorer.exe", "/select,\"" + fileName + "\"");
        }

        private void iCRComparetoggleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            iCRComparetoggleToolStripMenuItem.Checked = !iCRComparetoggleToolStripMenuItem.Checked;
            UpdateTopicView();
        }

        private void saveAllContentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileName = GetFilename(_fileName + ".content.txt");

            if (fileName == null)
            {
                return;
            }

            using (StreamWriter sw = new StreamWriter(fileName))
            {
                foreach (Topic t in topics)
                {
                    if (PassesFilter(t))
                    {
                        sw.WriteLine(Regex.Replace(t.post, "<[^>]*>", ""));
                    }
                }
            }

            Process.Start("explorer.exe", "/select,\"" + fileName + "\"");
        }
    }
}
