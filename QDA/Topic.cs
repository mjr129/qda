﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QDA
{
    class Topic
    {
        private class TopicContent
        {
            public readonly string id;
            public readonly string url;
            public readonly string title;
            public readonly string user;
            public readonly string post;
            public readonly int index;

            public TopicContent(string id, string url, string title, string user, string post, int index)
            {
                this.id = id;
                this.url = url;
                this.title = title;
                this.user = user;
                this.post = post;
                this.index = index;
            }
        }

        private TopicContent content;
        public List<string> yescodes = new List<string>();
        public List<string> nocodes = new List<string>();

        public string id { get { return content.id; } }
        public string url { get { return content.url; } }
        public string title { get { return content.title; } }
        public string user { get { return content.user; } }
        public string post { get { return content.post; } }
        public int index { get { return content.index; } }

        public Topic(string id, string url, string title, string user, string post, int index)
        {
            content = new TopicContent(id, url, title, user, post, index);
        }

        public Topic(Topic topic)
        {
            content = topic.content;
        }

        public CheckState GetCode(string code)
        {
            if (yescodes.Contains(code))
            {
                return CheckState.Checked;
            }
            else if (nocodes.Contains(code))
            {
                return CheckState.Unchecked;
            }
            else
            {
                return CheckState.Indeterminate;
            }
        }

        public void SetCode(string code, CheckState set)
        {
            if (set == CheckState.Checked)
            {
                // YES
                if (!yescodes.Contains(code))
                {
                    yescodes.Add(code);
                }

                if (nocodes.Contains(code))
                {
                    nocodes.Remove(code);
                }
            }
            else if (set == CheckState.Unchecked)
            {
                // NO
                if (yescodes.Contains(code))
                {
                    yescodes.Remove(code);
                }

                if (!nocodes.Contains(code))
                {
                    nocodes.Add(code);
                }
            }
            else if (set == CheckState.Indeterminate)
            {
                // MAYBE
                if (yescodes.Contains(code))
                {
                    yescodes.Remove(code);
                }

                if (nocodes.Contains(code))
                {
                    nocodes.Remove(code);
                }
            }
        }
    }
}
